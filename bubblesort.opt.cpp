/*
    BubbleSort (mull sortimine)
*/

#include <iostream>
using namespace std;

int main(){
    int array[10] = {23,4,63,12,8,77,28,22,65,50};
    //int array[10] = {4,8,12,22,23,28,50,63,65,77};
    int comparisons = 0;
    
    //mullsort
    bool operations = false;
    for(int j = 0;j < 9;j++){
        operations = false;
        for(int i= 0;i < (9-j);i++){
            comparisons++;
            if(array[i] > array[i+1]){
                operations = true;
                int swap = array[i];
                array[i] = array[i+1];
                array[i+1] = swap;
            }
            
        }
        if(!operations)break;
    }
    //prindime tulemuse
    for(int i = 0;i < 10;i++){
        cout << array[i] << " ";
    }
    cout << " ("<< comparisons <<")"<< endl;
}