#include <iostream>
#include <vector> // std::vector
#include <algorithm> // std::sort
using namespace std;

int main(){
    vector<int> array = {23,4,63,12,8,77,28,22,65,50};
    
    //sort(array.begin(), array.end());
    //equal_to, not_equal_to, greater, less, great_equal, less_equal
    sort(array.begin(), array.end(), greater<int>());
    sort(array.begin(), array.end(), less<int>());
    for(int i = 0;i < array.size();i++){
        cout << array[i] << " ";
    }
}