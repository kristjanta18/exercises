#include <iostream> // cout, cin
#include <fstream> // ifstream, ofstream
#include <vector> //std::vector<T>
using namespace std;

int main(){
    // fstream on võimeline nii lugema failidest (fstream::in)
    // kui kirjutama failidesse (fstream::out)
    fstream FILE;
    FILE.open("random.txt", fstream::in);
    
    vector<int> randomVector;
    int buf = 0;
    while(FILE >> buf){
        //lükkame uue väärtuse kuhjale peale
        randomVector.push_back(buf);
    }
    FILE.close();
    //vektori läbi käimine kasutades array lähenemist
    for(int i = 0;i < randomVector.size();i++){
        cout << "Nr "<< i << ": "<<randomVector[i] << endl;
    }
    
    //vektori läbi käimine kasutades modernset lähenemist
    for(vector<int>::iterator i = randomVector.begin();
    i != randomVector.end();i++){
        cout << "Nr: "<< *i << endl;
    }
}