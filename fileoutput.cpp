#include <iostream>
#include <fstream>
using namespace std;

int main(){
    //avab faili kirjutamiseks, kui ei eksisteeri, loob selle
    ofstream FILE;
    // ofstream::out sunnib FILE muutuja kirjutamise olekusse 
    // 
    FILE.open("output.txt", ofstream::out | ofstream::app);
    if(!FILE){
        cout << "ei saanud faili avada" <<endl;
    }
    FILE << "Rida 3 on siin" << endl;
    FILE << "Rida 4 on siin" << endl;
    
    FILE.close();
}