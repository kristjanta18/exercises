#include <iostream> // cout, cin
#include <fstream> // ifstream, ofstream
#include <cstdlib> // rand(), srand()
#include <ctime> //time()

using namespace std;

int main(){
    fstream FILE;
    FILE.open("random.txt", fstream::out);
    int errors = 0;
    
    if(!FILE){
        cout << "ERROR: could not open file" << endl;
        errors++;
    }
    int numbers = 0, random = 0;
    srand(time(0));
    
    cout << "Sisestage numbrite arv: ";
    cin >> numbers;
    for(int i = 1;i <= numbers;i++){
        random = rand() % 100 +1;
        FILE << random << " ";
    }
    FILE.close();
    if(errors == 0)cout << "generation successful"<<endl;
    else cout << "Errors: " << errors << endl;
}