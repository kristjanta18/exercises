#include <iostream>
using namespace std;

int main(){
    char array[10][10];
    
    // array fill
    for(int y = 0;y <10;y++){
        for(int x = 0;x < 10;x++){
            //tingimused
            if(x == y*2 || x+1==y*2 || x==y*2+1){
                array[x][y] = 'O';
            }
            else{
                array[x][y] = ' ';
            }
        }
    }
    
    // console output
    for(int y = 0;y <10;y++){
        for(int x = 0;x < 10;x++){
            cout << array[x][y] << " ";
        }
        cout << endl;
    }
}